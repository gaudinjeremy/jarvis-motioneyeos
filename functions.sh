#!/bin/bash

#MODULE MOTIONEYEOS

#necessite jarvis-dropbox
js_myo_getImgAndAlert() {

	camera=$1
	pathToImage=$2

	pathAnnee=$(echo $pathToImage | cut -c18-21)
	pathMois=$(echo $pathToImage | cut -c22-23)
	pathJour=$(echo $pathToImage | cut -c24-25)

	pathHeure=$(echo $pathToImage | cut -c26-27)
	pathMinute=$(echo $pathToImage | cut -c28-29)
	pathSeconde=$(echo $pathToImage | cut -c30-31)

	date="$pathAnnee-$pathMois-$pathJour"
	heure="$pathHeure-$pathMinute-$pathSeconde"

	dropBoxPath="Camera_$camera/$date/$heure.jpg"
	JarvisPath="plugins/jarvis-motioneyeos/files/$date-$heure-$camera.jpg"

	if [ ! -f $JarvisPath ]
	then
		jv_pg_dr_download_file "$dropBoxPath" "$JarvisPath" "True"
        heureAffiche=$pathHeure":"$pathMinute":"$pathSeconde
		js_ia_sendToMe "Mouvement détecté par la caméra $camera à $heureAffiche" "$JarvisPath"
	fi
}

#function qui efface le contenu de files
js_myo_deleteFiles(){

    delete=$(rm -rf plugins/jarvis-motioneyeos/files/*)
    js_ia_say "confirmation"
}
